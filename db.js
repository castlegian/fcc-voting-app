//jshint esversion:6

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const PollSchema = new Schema({
  owner: { type: mongoose.Schema.Types.ObjectId, ref: 'User'},
  question: { type: String, require: true },

  answer  : [
    {
      id  : Number,
      text: {type: String, require: true},
    }
  ],

  votes: Number,
  created_at: {type: Date, default: Date.now()},


});



module.exports = mongoose.model('Poll', PollSchema);
