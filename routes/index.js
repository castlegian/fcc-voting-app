//jshint esversion:6
const express = require('express');
const mongoose =  require('mongoose');
const passport =  require('passport');
const User =  require('../models/user');
const router = express.Router();

router.get('/', function(req, res, next) {
  res.render('index', {user: req.user});
});

router.post('/login', passport.authenticate('local'), function(req, res, next) {
    res.redirect('/');
});

router.post('/register', function(req, res, next) {
    User.register(new User({ username : req.body.rusername }), req.body.rpassword, function(error, user) {
        if (error) {
          res.render('index', {
            error: error.message
          });
        }
       passport.authenticate('local',
     {failureRedirect: '/'})(req, res, function () {
              req.session.save(function (error) {
                  if (err)
                      return next(error);

                  res.redirect('/');
              });
          });
    });
});

router.get('/logout', function(req, res) {
    req.logout();
    res.redirect('/');
});


module.exports = router;
