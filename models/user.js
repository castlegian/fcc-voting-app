//jshint esversion:6
const mongoose = require('mongoose');
const passportLocalMongoose = require('passport-local-mongoose');
const Schema = mongoose.Schema;

const UserSchema = new Schema({
  username  : {type: String, require: true, unique: true},
  password  : {type: String, require: true},

});
UserSchema.plugin(passportLocalMongoose);

module.exports = mongoose.model('User', UserSchema);
